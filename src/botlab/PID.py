# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time,signal
import lcm
import math,time 
import PID
from os import getcwd
from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_laser_t
#from lcmtypes import maebot_command_t


class PID():
    def __init__(self, kp, ki, kd):
        # Main Gains
        self.kp = kp
        self.ki = ki
        self.kd = kd
            
        # Integral Terms
        self.iTerm = 0.0
        self.iTermMin = -0.2
        self.iTermMax = 0.2

        # Input (feedback signal)
        self.prevInput = 0.0
        self.input = 0.0

        # Reference Signals (signal and derivative!)
        self.realdotsetpoint = 0.0
        self.dotsetpoint = 0.0          
        self.setpoint = 0.0
        self.target = 0.0
        # Output signal and limits
        self.output = 0.0
        self.outputMin = -0.4
        self.outputMax = 0.4
        self.negtrim = True
        # Update Rate
        self.updateRate = 0.1

        # Error Signals
        self.error = 0.0
        self.errordot = 0.0
        
        self.trim = 0.0
        self.time = 0.0
        self.ptime = 0.0
        self.exit = 0
    def Compute(self):
        # IMPLEMENT ME!
        # Different then last project! 
        # Now you have a speed reference also!
        # Also Update Rate can be gone here or in SetTunnings function
        # (it is your choice!) But change functions consistently  
        self.setpoint = self.setpoint + (self.dotsetpoint * self.updateRate)
        
        self.error = self.setpoint - self.input
        #print 'error'
        #print self.error
        if self.updateRate != 0:
            #self.realdotsetpoint = self.realdotsetpoint + 0.001 * self.dotsetpoint
            #if(math.fabs(self.realdotsetpoint) > math.fabs(self.dotsetpoint)):
            self.realdotsetpoint = self.dotsetpoint
            self.errordot = self.realdotsetpoint - (self.input - self.prevInput)/self.updateRate
            print (self.input - self.prevInput)/self.updateRate
        else:
            self.realdotsetpoint = self.dotsetpoint
        self.errordot = self.realdotsetpoint
        self.iTerm = self.iTerm + self.ki * self.error
        if self.iTerm > self.iTermMax:
            self.iTerm = self.iTermMax
        elif self.iTerm < self.iTermMin:
            self.iTerm = self.iTermMin 
        self.output = self.kp * self.error + self.kd * (self.errordot) + self.iTerm + self.trim #trim
        print 'output:'
        print self.output
        self.time = self.time + self.updateRate
        if self.output > self.outputMax:
            self.output = self.outputMax
        if self.output < self.outputMin:
            self.output = self.outputMin
        
        if (self.setpoint >= self.target and self.target > 0) or (self.setpoint <= self.target and self.target < 0):
            self.dotsetpoint = 0
            self.ptime = self.ptime + 1
            if(self.negtrim):
                self.trim = self.trim * -0.78
                self.negtrim = False
            print self.trim
            if(self.ptime > 100):
                self.iTerm = 0
                self.exit = 1
        self.prevInput = self.input
        print 'speed:'
        
    # Accessory Function to Change Gains
    # Update if you are not using updateRate in Compute()
    def SetTunings(self, kp, ki, kd):
        self.kp = kp
        self.ki = ki 
        self.kd = kd

    def SetIntegralLimits(self, imin, imax):
        self.iTermMin = imin
        self.iTermMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax
                
    def SetUpdateRate(self, rateInSec):
        self.updateRate = rateInSec



class PIDController():
    def __init__(self):
        self.tmp = True
        # Create Both PID
        self.leftCtrl =  PID(0.012,0.00005,0.001)
        self.rightCtrl = PID(0.012,0.00005,0.001)
        # LCM Subscribe
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", 
                                        self.motorFeedbackHandler)
        signal.signal(signal.SIGINT, self.signal_handler)
        self.publishLaser(0)

        # Odometry 
        self.wheelDiameterMillimeters = 32.0  # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0     # separation of wheels [mm]    
        self.ticksPerRev = 16.0           # encoder tickers per motor revolution
        self.gearRatio = 30.0             # 30:1 gear ratio
        self.enc2mm = ((math.pi * self.wheelDiameterMillimeters) / 
                       (self.gearRatio * self.ticksPerRev)) 
                        # encoder ticks to distance [mm]

        # Initialization Variables
        self.InitialPosition = (0.0, 0.0)
        self.oldTime = 0.0
        self.startTime = 0.0
        self.currTick = (0,0)
        self.prevTick = (0,0)
        self.totalTick = (0,0)
        self.realTime = 0.0
        self.velScale = 1.0
        self.timeDiff = 0.0
        self.leftCtrl.setPoint = 0.0
        self.rightCtrl.setPoint = 0.0
        self.leftCtrl.trim = 0.0
        self.rightCtrl.trim = 0.0
        self.rEncData = open('rightEncoder.csv','w')
        self.lEncData = open('leftEncoder.csv','w')
        self.lOutput = open('leftOutput.csv','w')
        self.rOutput = open('rightOutput.csv','w')
        self.timer = open('time.csv','w')
    def publishMotorCmd(self):
        cmd = maebot_diff_drive_t()
        cmd.motor_left_speed = self.velScale * self.leftCtrl.output
        cmd.motor_right_speed = self.velScale * self.rightCtrl.output
        print cmd.motor_left_speed,cmd.motor_right_speed
        self.lOutput.write(str(cmd.motor_left_speed)+'\n')
        self.rOutput.write(str(cmd.motor_right_speed)+'\n')
        self.lEncData.write(str(self.totalTick[0])+',')
        self.rEncData.write(str(self.totalTick[1])+',')
        self.timer.write(str(self.leftCtrl.time) + ',')
        self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        # IMPLEMENT ME

    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)
        self.prevTick = self.currTick
        self.currTick = (msg.encoder_left_ticks,msg.encoder_right_ticks)
        self.oldTime = self.startTime
        self.startTime = msg.utime
        if self.oldTime != 0:
            self.timeDiff = (self.startTime - self.oldTime) / 1000000.0
        if self.prevTick != (0,0):
            self.totalTick = (self.totalTick[0] + self.currTick[0] - self.prevTick[0],self.totalTick[1] + self.currTick[1] - self.prevTick[1])
        self.leftCtrl.input = self.totalTick[0] * self.enc2mm
        self.rightCtrl.input = self.totalTick[1] * self.enc2mm #update
        self.leftCtrl.updateRate = self.timeDiff
        self.rightCtrl.updateRate = self.timeDiff
        
        self.leftCtrl.Compute()
        self.rightCtrl.Compute()
        if(self.leftCtrl.exit and self.rightCtrl.exit and self.tmp):
        	self.lEncData.close()
        	self.rEncData.close()
        	self.lOutput.close()
        	self.rOutput.close()
        	print 'path:', getcwd()
        	self.tmp = False
        	exit(0)
        self.publishMotorCmd()
        # IMPLEMENT ME
    
    def Controller(self):

        # For now give a fixed command here
        # Later code to get from groundstation should be used
        DesiredSpeed =  40
        DesiredDistance =  math.pi * self.axleLengthMillimeters * (90/360.0)
        self.leftCtrl.SetTunings(0.0008,0.00005,0.00001)
        self.rightCtrl.SetTunings(0.0008,0.00005,0.00001)
        self.leftCtrl.target = -DesiredDistance
        self.leftCtrl.dotsetpoint =-DesiredSpeed
        self.rightCtrl.target = DesiredDistance
        self.rightCtrl.dotsetpoint = DesiredSpeed

        self.leftCtrl.trim = -0.150#0.001 * 0.2 * DesiredSpeed
        self.rightCtrl.trim = 0.152#0.001 * 0.2 * DesiredSpeed
        while(1):
            self.lc.handle()
        # IMPLEMENT ME
        # MAIN CONTROLLER
            
    def publishLaser(self,x):
        # IMPLEMENT ME
        # TASK: PUBLISHES BOT_ODO_VEL MESSAGE
        msg = maebot_laser_t()
        msg.laser_power = x
        self.lc.publish("MAEBOT_LASER",msg.encode())      

        
    # Function to print 0 commands to morot when exiting with Ctrl+C 
    # No need to change 
    def signal_handler(self,signal, frame):
        print("Terminating!")
        for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0  
            self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        exit(1)

pid = PIDController()
pid.Controller()

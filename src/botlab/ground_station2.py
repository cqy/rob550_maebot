import sys, os, time
import lcm
import math

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
 
import matplotlib.backends.backend_agg as agg

import pygame
from pygame.locals import *

from lcmtypes import maebot_diff_drive_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
# NEEDS TO ADD OTHER USED LCM TYPES!!!

class MainClass:
  def __init__(self, width=640, height=480, FPS=10):
    pygame.init()
    self.width = width
    self.height = height
    self.screen = pygame.display.set_mode((self.width, self.height))
    self.x = 0
    self.y = 0
    self.t = 0
    self.dxy = 0
    self.dth = 0
    self.dt = 0
    
    
    # LCM Subscribe
    self.lc = lcm.LCM()
    lcmOdoPos = self.lc.subscribe("BOT_ODO_POSE",self.OdoPositionHandler)
    lcmOdoVel = self.lc.subscribe("BOT_ODO_VEL", self.OdoVelocityHandler)
	# NEEDS TO SUBSCRIBE TO OTHER LCM CHANNELS LATER!!!

    # Prepare Figure for Lidar
    self.fig = plt.figure(figsize=[3, 3], # Inches
                        dpi=100,  # 100 dots per inch, so the resulting buffer is 400x400 pixels
                      )
    self.fig.patch.set_facecolor('white')
    self.fig.add_axes([0,0,1,1],projection='polar')
    self.ax = self.fig.gca()

    # Prepare Figures for control
    # TO BE ADDED LATER
    
  def OdoVelocityHandler(self,channel,data): 
  	OdoVelMsg = odo_dxdtheta_t.decode(data)  
  	self.dxy = OdoVelMsg.dxy
  	self.dth = OdoVelMsg.dtheta
  	self.dt = OdoVelMsg.dt

  def OdoPositionHandler(self,channel,data): 
	OdoPosMsg = odo_pose_xyt_t.decode(data)
	self.x = OdoPosMsg.xyt[0]
	self.y = OdoPosMsg.xyt[1]
	self.t = OdoPosMsg.xyt[2]
 

    # IMPLEMENT ME !!!
 
#  def LidarHandler(self,channel,data): 
    # IMPLEMENT ME !!!
 
  def MainLoop(self):
    pygame.key.set_repeat(1, 20)
    vScale = 0.5

    # Prepare Text to Be output on Screen
    font = pygame.font.SysFont("DejaVuSans Mono",14)

    while 1:
      leftVel = 0
      rightVel = 0
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          sys.exit()
        elif event.type == KEYDOWN:
          if ((event.key == K_ESCAPE)
          or (event.key == K_q)):
            sys.exit()
          key = pygame.key.get_pressed()
    	  if key[K_RIGHT]:
            leftVel = leftVel + 0.35
            rightVel = rightVel - 0.35
          if key[K_LEFT]:
            leftVel = leftVel - 0.40
            rightVel = rightVel + 0.40
          if key[K_UP]:
            leftVel = leftVel + 0.65
            rightVel = rightVel + 0.65
          if key[K_DOWN]:
            leftVel = leftVel - 0.65
            rightVel = rightVel - 0.65            
      print "(L:%f R:%f)" % (leftVel, rightVel)

      cmd = maebot_diff_drive_t()
      cmd.motor_right_speed = vScale * rightVel
      cmd.motor_left_speed = vScale * leftVel
      #self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())

      self.screen.fill((255,255,255))

      # Plot Lidar Scans
      # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
      plt.cla()
      self.ax.plot(0,0,'or',markersize=2)
      self.ax.set_rmax(1.5)
      self.ax.set_theta_direction(-1)
      self.ax.set_theta_zero_location("N")
      self.ax.set_thetagrids([0,45,90,135,180,225,270,315],labels=['','','','','','','',''], frac=None,fmt=None)
      self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],angle=None,fmt=None)

      canvas = agg.FigureCanvasAgg(self.fig)
      canvas.draw()
      renderer = canvas.get_renderer()
      raw_data = renderer.tostring_rgb()
      size = canvas.get_width_height()
      surf = pygame.image.fromstring(raw_data, size, "RGB")
      self.screen.blit(surf, (320,0))

      # Position and Velocity Feedback Text on Screen
      self.lc.handle()      
      pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
      text = font.render("  POSITION  ",True,(0,0,0))
      self.screen.blit(text,(10,360))
      xpos = "x  " + ":%.3f"%(self.x)+"[mm]"
      text = font.render(xpos,True,(0,0,0))
      self.screen.blit(text,(10,390))
      ypos = "y  " + ":%.3f"%(self.y)+"[mm]"
      text = font.render(ypos,True,(0,0,0))
      self.screen.blit(text,(10,420))
      thetapos = "t  " + ":%.3f"%(self.t / math.pi * 180) + "[deg]"
      text = font.render(thetapos,True,(0,0,0))
      self.screen.blit(text,(10,450))
      
      text = font.render("  VELOCITY  ",True,(0,0,0))
      self.screen.blit(text,(150,360))
      xyVel = "dxy/dt " + ":%.3f"%(self.dxy) + "[mm/s]"
      text = font.render(xyVel,True,(0,0,0))
      self.screen.blit(text,(150,390))
      theta = "dth/dt: " + ":%.3f"%(self.dth) + "[deg/s]"
      text = font.render(theta,True,(0,0,0))
      self.screen.blit(text,(150,420))
      timemsg = "dt:   " + ":%.3f"%(self.dth) + "[s]"
      text = font.render(timemsg,True,(0,0,0))
      self.screen.blit(text,(150,450))

      pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()

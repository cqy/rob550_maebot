import sys, os, time
import lcm
import math

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.backends.backend_agg as agg
import pygame
from pygame.locals import *

from lcmtypes import maebot_diff_drive_t
from lcmtypes import velocity_cmd_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import pid_init_t
from lcmtypes import rplidar_laser_t
# Callling recently included files
from laser import *
from slam import *
from maps import *
from guidance import *
from astar import *
from scale import *
from execute import *
# SLAM preferences
# CHANGE TO OPTIMIZE
USE_ODOMETRY = True
MAP_QUALITY = 5

# Laser constants
# CHANGE TO OPTIMIZE IF NECSSARY
DIST_MIN = 30; # minimum distance
DIST_MAX = 6000; # maximum distance

# Map constants
# CHANGE TO OPTIMIZE
MAP_SIZE_M = 3.0 # size of region to be mapped [m]
INSET_SIZE_M = 1.0 # size of relative map
MAP_RES_PIX_PER_M = 100 # number of pixels of data per meter [pix/m]
MAP_SIZE_PIXELS = int(MAP_SIZE_M*MAP_RES_PIX_PER_M) # number of pixels across the entire map
MAP_DEPTH = 5 # depth of data points on map (levels of certainty)

# CONSTANTS
DEG2RAD = math.pi / 180
RAD2DEG = 180 / math.pi

# KWARGS
# PASS TO MAP AND SLAM FUNCTIONS AS PARAMETER
KWARGS, gvars = {}, globals()
for var in ['MAP_SIZE_M','INSET_SIZE_M','MAP_RES_PIX_PER_M','MAP_DEPTH','USE_ODOMETRY','MAP_QUALITY']:
  KWARGS[var] = gvars[var] # constants required in modules
class MainClass:
    def __init__(self, width=640, height=480, FPS=10):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.x = 0
        self.y = 0
        self.t = 0  
        self.slamx = 0
        self.slamy = 0
        self.slamt = 0
        self.dxy = 0
        self.dth = 0
        self.dt = 0
        self.time = 0
        self.slamtime = 0
        self.surf = 0
        self.laser = RPLidar(DIST_MIN, DIST_MAX) # lidar
        self.map = DataMatrix(**KWARGS) # handle map data
        self.slam = Slam(self.laser, **KWARGS) # do slam processing
        self.nranges = 0
        self.ranges = 0
        self.rangesmm = 0
        self.thetas = 0
        self.thetasdeg = 0
        self.times = 0
        self.botPos = 0
        self.nintensities = 0
        self.intensities = 0
        self.mapthetas = np.arange(360)
        self.mapthetas = tuple(self.mapthetas)
        self.mapranges = np.zeros(360)
        self.initial = True
        self.startPoint = 0
        self.endPoint = 0
        # LCM Subscribe
        self.lc = lcm.LCM()
        # NEEDS TO SUBSCRIBE TO OTHER LCM CHANNELS LATER!!!

        # Prepare Figure for Lidar
        self.fig = plt.figure(figsize=[3, 3], # Inches
                              dpi=100)    # 100 dots per inch, 
        self.fig.patch.set_facecolor('white')
        self.fig.add_axes([0,0,1,1],projection='polar')
        self.ax = self.fig.gca()

        # Prepare Figures for control
        path = os.path.realpath(__file__)
        path = path[:-17] + "maebotGUI/"

        self.arrowup = pygame.image.load(path + 'fwd_button.png')
        self.arrowdown = pygame.image.load(path + 'rev_button.png')
        self.arrowleft = pygame.image.load(path + 'left_button.png')
        self.arrowright = pygame.image.load(path + 'right_button.png')
        self.resetbut = pygame.image.load(path + 'reset_button.png')
        self.mapimage = pygame.image.load("map_images/" + 'map.png')
        self.arrows = [0,0,0,0]

        # PID Initialization - Change for your Gains!
        command = pid_init_t()
        command.kp = 0.0        
        command.ki = 0.0
        command.kd = 0.0
        command.iSat = 0.0 # Saturation of Integral Term. 
                           # If Zero shoudl reset the Integral Term
        lcmOdoPos = self.lc.subscribe("BOT_ODO_POSE",self.OdoPositionHandler)
        lcmOdoVel = self.lc.subscribe("BOT_ODO_VEL", self.OdoVelocityHandler)
        lcmRplidar = self.lc.subscribe("RPLIDAR_LASER", self.LidarHandler)
        self.lc.publish("GS_PID_INIT",command.encode())
    def OdoVelocityHandler(self,channel,data): 
        OdoVelMsg = odo_dxdtheta_t.decode(data)  
        #self.dxy += OdoVelMsg.dxy
        #self.dth += OdoVelMsg.dtheta
        #self.dt += OdoVelMsg.dt
        self.dxy = OdoVelMsg.dxy
        self.dth = OdoVelMsg.dtheta
        self.dt = OdoVelMsg.dt
        
    def OdoPositionHandler(self,channel,data): 
        OdoPosMsg = odo_pose_xyt_t.decode(data)
        self.x = OdoPosMsg.xyt[0]
        self.y = OdoPosMsg.xyt[1]
        self.t = OdoPosMsg.xyt[2]
        self.time = OdoPosMsg.utime / 1000000
       
    def LidarHandler(self,channel,data):
        j = 0
        self.mapranges = np.zeros(360)
        lidarMsg = rplidar_laser_t.decode(data)
        self.nranges = lidarMsg.nranges
        self.ranges = lidarMsg.ranges
        self.rangesmm = [int(x * 1000) for x in self.ranges]
        self.thetas = lidarMsg.thetas
        self.thetasdeg = [int(y * 180 / math.pi) for y in self.thetas]
        self.mapranges = tuple(self.mapranges)
        data = zip(self.rangesmm,self.thetasdeg)
        dxy = np.sqrt(np.square(self.x - self.slamx) + np.square(self.y - self.slamy))
        dth = self.t - self.slamt
        dt = self.time - self.slamtime
        self.slamx = self.x
        self.slamy = self.y
        self.slamt = self.t
        self.slamtime = self.time
        self.botPos = self.slam.updateSlam(data,(dxy,dth,dt))
        self.dxy = 0
        self.dth = 0
        self.dt = 0
        if self.initial:
            self.map.getRobotPos(self.botPos,True)
            self.initial = False
        else:
            self.map.getRobotPos(self.botPos,False)
        self.map.drawBreezyMap(self.slam.getBreezyMap())
        self.map.saveImage()
        self.mapimage = pygame.image.load("map_images/" + 'map.png')
    def MainLoop(self):
        pygame.key.set_repeat(1, 20)
        vScale = 0.5

        # Prepare Text to Be output on Screen
        font = pygame.font.SysFont("DejaVuSans Mono",14)

        while 1:
            leftVel = 0.0
            rightVel = 0.0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == KEYDOWN:
                    if ((event.key == K_ESCAPE)
                    or (event.key == K_q)):
                        sys.exit()
                    key = pygame.key.get_pressed()
                    if key[K_RIGHT]:
                        leftVel = leftVel + 0.45
                        rightVel = rightVel - 0.45
                    elif key[K_LEFT]:
                        leftVel = leftVel - 0.45
                        rightVel = rightVel + 0.45
                    elif key[K_UP]:
                        leftVel = leftVel + 0.60
                        rightVel = rightVel + 0.60
                    elif key[K_DOWN]:
                        leftVel = leftVel - 0.60
                        rightVel = rightVel - 0.60
                    elif key[K_a]:
                        print 'record start point'
                        self.startPoint = self.botPos
                        #self.startPoint = (1385.6821219454093, 2478.7264548666653, 28.93163241209959)
                        #self.endPoint = (1421.9080557477996, 514.223335545374, 4.434061596643969)
                        print self.startPoint
                        np.savetxt('data.txt',self.map.mapMatrix,delimiter = ',',fmt='%1.1f')
                        s = Scale()
                        s.scale()
                        a = astar(self.startPoint,self.endPoint)
                        a.createMap()
                        a.compute()
                        print 'commandgot!'
                        e = execute(a.pathCommand())
                        e.execute()
                      
                    elif key[K_s]:
                        print 'record end point'
                        self.endPoint = self.botPos
                        print self.endPoint
                    else:
                        leftVel = 0.0
                        rightVel = 0.0                      
                    cmd = maebot_diff_drive_t()
                    cmd.motor_right_speed = vScale * rightVel
                    cmd.motor_left_speed = vScale * leftVel
                            
                    self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    cmd1 = velocity_cmd_t()
                    if event.button == 1:
                        if ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            cmd1.fwd_vel = 100
                            cmd1.fwd_dis = 200
                            cmd1.ang_vel = 0
                            self.lc.publish("GS_VELOCITY_CMD",cmd1.encode())
                            print "Commanded PID Forward One Meter!"
                        elif ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            cmd1.fwd_vel = -100
                            cmd1.fwd_dis = -200
                            cmd1.ang_vel = 0
                            self.lc.publish("GS_VELOCITY_CMD",cmd1.encode())
                            print "Commanded PID Backward One Meter!"
                        elif ((event.pos[0] > 363) and (event.pos[0] < 435) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            cmd1.fwd_vel = 150
                            cmd1.fwd_dis = 0
                            cmd1.ang_vel = 30
                            self.lc.publish("GS_VELOCITY_CMD",cmd1.encode())
                            print "Commanded PID Left One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            cmd1.fwd_vel = 100
                            cmd1.fwd_dis = 0
                            cmd1.ang_vel = -30
                            self.lc.publish("GS_VELOCITY_CMD",cmd1.encode())
                            print "Commanded PID Right One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            pid_cmd = pid_init_t()
                            pid_cmd.kp = 0.0        # CHANGE FOR YOUR GAINS!
                            pid_cmd.ki = 0.0        # See initialization
                            pid_cmd.kd = 0.0
                            pid_cmd.iSat = 0.0
                            self.lc.publish("GS_PID_INIT",pid_cmd.encode())
                            print "Commanded PID Reset!"
                            self.map.saveImage()
                            np.savetxt('data.txt',self.map.mapMatrix,delimiter = ',',fmt='%1.1f')
                           
                            
                            #print self.map.mapMatrix
                            

            self.screen.fill((255,255,255))

            # Plot Lidar Scans
            # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
            plt.cla()
            self.ax.plot(self.thetas,self.ranges,'or',markersize=2)   
            self.ax.plot(1,0,'or',markersize=2)
            self.ax.set_rmax(1.5)
            self.ax.set_theta_direction(-1)
            self.ax.set_theta_zero_location("N")
            self.ax.set_thetagrids([0,45,90,135,180,225,270,315],
                                    labels=['','','','','','','',''], 
                                    frac=None,fmt=None)
            self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],
                                    angle=None,fmt=None)

            canvas = agg.FigureCanvasAgg(self.fig)
            canvas.draw()
            renderer = canvas.get_renderer()
            raw_data = renderer.tostring_rgb()
            size = canvas.get_width_height()
            surf = pygame.image.fromstring(raw_data, size, "RGB")
            self.screen.blit(surf, (320,0))

            # Position and Velocity Feedback Text on Screen
            self.lc.handle()          
            pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
            text = font.render("  POSITION  ",True,(0,0,0))
            self.screen.blit(text,(10,360))
            xpos = "x  " + ":%.3f"%(self.x)+"[mm]"
            text = font.render(xpos,True,(0,0,0))
            self.screen.blit(text,(10,390))
            ypos = "y  " + ":%.3f"%(self.y)+"[mm]"
            text = font.render(ypos,True,(0,0,0))
            self.screen.blit(text,(10,420))
            thetapos = "t  " + ":%.3f"%(self.t / math.pi * 180) + "[deg]"
            text = font.render(thetapos,True,(0,0,0))
            self.screen.blit(text,(10,450))
              
            text = font.render("  VELOCITY  ",True,(0,0,0))
            self.screen.blit(text,(150,360))
              
            text = font.render("  VELOCITY  ",True,(0,0,0))
            self.screen.blit(text,(150,360))
            xyVel = "dxy/dt " + ":%.3f"%(self.dxy) + "[mm/s]"
            text = font.render(xyVel,True,(0,0,0))
            self.screen.blit(text,(150,390))
            theta = "dth/dt: " + ":%.3f"%(self.dth) + "[deg/s]"
            text = font.render(theta,True,(0,0,0))
            self.screen.blit(text,(150,420))
            timemsg = "dt:   " + ":%.3f"%(self.dt) + "[s]"
            text = font.render(timemsg,True,(0,0,0))
            self.screen.blit(text,(150,450))
            # Plot Buttons
            self.screen.blit(self.arrowup,(438,325))
            self.screen.blit(self.arrowdown,(438,400))
            self.screen.blit(self.arrowleft,(363,400))
            self.screen.blit(self.arrowright,(513,400))
            self.screen.blit(self.resetbut,(513,325))
            self.screen.blit(self.mapimage,(10,10))
            pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()

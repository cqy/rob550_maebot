# odometry.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
import math

from breezyslam.robots import WheeledRobot

from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_sensor_data_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t

class Maebot(WheeledRobot):
    
  def __init__(self):
    self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
    self.axleLengthMillimeters = 80.0        # separation of wheels [mm]  
    self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
    self.gearRatio = 30.0                    # 30:1 gear ratio
    self.enc2mm = 3.141592 * (self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]

    self.prevEncPos = (0,0,0)           # store previous readings for odometry
    self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
    self.currEncPos = (0,0,0)           # current reading for odometry left,right,time
    self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
    self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]
    self.gyro = (0,0,0)
    self.first = True
    self.firstl = 0
    self.firstr = 0
    self.thetaThresh = 200
    WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

    # LCM Initialization and Subscription
    self.lc = lcm.LCM()
    lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
    lcmSensorSub = self.lc.subscribe("MAEBOT_SENSOR_DATA", self.sensorDataHandler)
  
  def calcVelocities(self):
    # IMPLEMENT ME
    # TASK: CALCULATE VELOCITIES FOR ODOMETRY
    # Update self.currOdoVel and self.prevOdoVel
    dt = float((self.currEncPos[2] - self.prevEncPos[2]) / 1000000.0)
    dxy = math.sqrt(math.pow((self.currOdoPos[0] - self.prevOdoPos[0]),2) + math.pow((self.currOdoPos[1] - self.prevOdoPos[1]),2)) / dt
    dtheta = (self.currOdoPos[2] - self.prevOdoPos[2]) / dt
    self.currOdoVel = (dxy, dtheta, dt)
  def getVelocities(self):
    # IMPLEMENT ME
    # TASK: RETURNS VELOCITY TUPLE
    # Return a tuple of (dxy [mm], dtheta [rad], dt [s])


    return self.currOdoVel#(dxy, dtheta, dt) # [mm], [rad], [s]

  def calcOdoPosition(self):
    # IMPLEMENT ME
    # TASK: CALCULATE POSITIONS
    # Update self.currOdoPos and self.prevOdoPos
    self.prevOdoPos = self.currOdoPos
    dt = float((self.currEncPos[2] - self.prevEncPos[2]) / 1000000.0)
    a = (self.currEncPos[0] - self.prevEncPos[0]) * self.enc2mm #left wheel distance
    b = (self.currEncPos[1] - self.prevEncPos[1]) * self.enc2mm #right wheel distance
    deltaAngle = ((a-b)/self.axleLengthMillimeters)/dt
    errorDiff = math.fabs(self.gyro[2] - deltaAngle)
    if errorDiff > self.thetaThresh:
    	angle = self.gyro[2] * dt
    	print 'error detected!'
    else:
    	angle = deltaAngle*dt


    if a == b:
        self.currOdoPos = (self.prevOdoPos[0] + math.sin(self.prevOdoPos[2]) * a,self.prevOdoPos[1] + math.cos(self.prevOdoPos[2]) * a,self.prevOdoPos[2])     #update theta
    elif (a>0 and b>0 and a>b) or (a<0 and b<0 and a<b):
        an = abs(angle)
        tx=(abs(a)/an - self.axleLengthMillimeters/2)*(1-math.cos(angle))
        ty=(abs(a)/an - self.axleLengthMillimeters/2)*math.sin(angle)
        print tx,ty,angle,a,b
        self.currOdoPos = (self.prevOdoPos[0]+tx*math.cos(self.prevOdoPos[2])+ty*math.sin(self.prevOdoPos[2]),self.prevOdoPos[1]-tx*math.sin(self.prevOdoPos[2])+ty*math.cos(self.prevOdoPos[2]),self.prevOdoPos[2]+angle)
    elif (a>0 and b>0 and a<b) or (a<0 and b<0 and a>b):
        an = abs(angle)
        tx=-1*(abs(b)/an - self.axleLengthMillimeters/2)*(1-math.cos(-angle))
        ty=(abs(b)/an - self.axleLengthMillimeters/2)*math.sin(-angle)
        print tx,ty,angle,a,b
        print'a'
        self.currOdoPos = (self.prevOdoPos[0]+tx*math.cos(self.prevOdoPos[2])+ty*math.sin(self.prevOdoPos[2]),self.prevOdoPos[1]-tx*math.sin(self.prevOdoPos[2])+ty*math.cos(self.prevOdoPos[2]),self.prevOdoPos[2]+angle)
    
    elif a == -b:
        self.currOdoPos = (self.prevOdoPos[0],self.prevOdoPos[1],self.prevOdoPos[2]+2*a/self.axleLengthMillimeters)
    
    elif (a>0 and b<0 and abs(a)<abs(b)) or (a<0 and b>0 and abs(a)<abs(b)):
        h= self.axleLengthMillimeters*0.5*(abs(b)-abs(a))/(abs(a)+abs(b))
        ang = -b/(h + self.axleLengthMillimeters/2)
        tx = math.cos(ang)*h-h
        ty = -math.sin(ang)*h
        self.currOdoPos = (self.prevOdoPos[0]+tx*math.cos(self.prevOdoPos[2])+ty*math.sin(self.prevOdoPos[2]),self.prevOdoPos[1]-tx*math.sin(self.prevOdoPos[2])+ty*math.cos(self.prevOdoPos[2]),self.prevOdoPos[2]+ang)
        
    elif (a>0 and b<0 and abs(a)>abs(b)) or (a<0 and b>0 and abs(a)>abs(b)):
        h= self.axleLengthMillimeters*0.5*(abs(a)-abs(b))/(abs(a)+abs(b))
        ang = a/(h + self.axleLengthMillimeters/2)
        tx = h-math.cos(ang)*h
        ty = math.sin(ang)*h
        self.currOdoPos = (self.prevOdoPos[0]+tx*math.cos(self.prevOdoPos[2])+ty*math.sin(self.prevOdoPos[2]),self.prevOdoPos[1]-tx*math.sin(self.prevOdoPos[2])+ty*math.cos(self.prevOdoPos[2]),self.prevOdoPos[2]+ang)
                 

  def getOdoPosition(self):
    # IMPLEMENT ME
    # TASK: RETURNS POSITION TUPLE
    # Return a tuple of (x [mm], y [mm], theta [rad])
    return (x, y, theta) # [mm], [rad], [s]

  def publishOdometry(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_POSE MESSAGE
     odoMsg = odo_pose_xyt_t()
     odoMsg.utime = self.currEncPos[2]
     odoMsg.xyt = self.currOdoPos
     self.lc.publish("BOT_ODO_POSE",odoMsg.encode())
     #print 'pos:'
     #print self.currOdoPos
  def publishVelocities(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_VEL MESSAGE
    velMsg = odo_dxdtheta_t()
    velMsg.dxy = self.currOdoVel[0]
    velMsg.dtheta = self.currOdoVel[1]
    velMsg.dt = self.currOdoVel[2]
    self.lc.publish("BOT_ODO_VEL",velMsg.encode())
  def motorFeedbackHandler(self,channel,data):
    msg = maebot_motor_feedback_t.decode(data)
    self.prevEncPos = self.currEncPos;
    if self.first:
      self.currEncPos = (0,0,msg.utime)
      self.firstl = msg.encoder_left_ticks
      self.firstr = msg.encoder_right_ticks   
      self.first = False
    else:	    
      self.currEncPos = (msg.encoder_left_ticks-self.firstl,msg.encoder_right_ticks-self.firstr,msg.utime)
    self.calcOdoPosition()
    self.calcVelocities()
    # IMPLEMENT ME
    # TASK: PROCESS ENCODER DATA
    # get encoder positions and store them in robot,
    # update robots position and velocity estimate

  def sensorDataHandler(self,channel,data):
    msg = maebot_sensor_data_t.decode(data)
    gyroRes = float(2*250 /65536.0)
    resTup = (gyroRes,gyroRes,gyroRes)
    self.gyro = tuple(gyroRes * x for x in msg.gyro)
    # IMPLEMENT ME
    # TASK: PROCESS GYRO DATA

  def MainLoop(self):
    oldTime = time.time()
    frequency = 20;
    while(1):
      self.lc.handle()
      if(time.time()-oldTime > 1.0/frequency):
        self.publishOdometry()
        self.publishVelocities()
        oldTime = time.time()   


if __name__ == "__main__":
  
  robot = Maebot()
  robot.MainLoop()  


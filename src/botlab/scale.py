import numpy as np
#import matplotlib.pyplot as plt
DIM = 300
DIMNEW = 50
SCALEFACTOR = DIM/DIMNEW

class Scale:

    def __init__(self):
        self.DIM = 0
        self.DIMNEW = 0
    def scale(self):
        data = np.loadtxt('data.txt',delimiter = ',')
        newdata = np.zeros((DIMNEW,DIMNEW))
        for x in range (0,DIM):
            for y in range (0,DIM):
                a = int(x / SCALEFACTOR)
                b = int(y / SCALEFACTOR)
                newdata[a][b] =  newdata[a][b] + data[x][y]  
        for x in range (0,DIMNEW):
            for y in range (0,DIMNEW):
                if newdata[x][y] > 235 * SCALEFACTOR * SCALEFACTOR:
                    newdata[x][y] = 0
                else:
                    newdata[x][y] = 1
        np.savetxt('scale.txt',newdata,delimiter = ',',fmt='%1.1d')


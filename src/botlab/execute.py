import sys, os, time
import lcm
import math
import numpy as np
from lcmtypes import maebot_diff_drive_t
from lcmtypes import velocity_cmd_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import pid_init_t
from lcmtypes import rplidar_laser_t
# Callling recently included files
from laser import *
from slam import *
from maps import *
from guidance import *
from astar import *
from scale import *

class execute:
    def __init__(self, list):
        print list
        self.cmdList = list
        self.lc = lcm.LCM()
    def execute(self):
        print 'start execution'        
        for cmd in self.cmdList:
            print 'getCommand'
            command = velocity_cmd_t()
            if cmd[0] != 0:
                command.fwd_vel = 100 * cmd[0] / abs(cmd[0])
            else:
                command.fwd_vel = 0
            command.fwd_dis = cmd[0]*60
            command.ang_vel = cmd[1]
            print command.fwd_dis,command.ang_vel
            self.lc.publish("GS_VELOCITY_CMD",command.encode())
            if cmd[0] != 0:
                x = abs(command.fwd_dis/command.fwd_vel) + 1
                print x
                time.sleep(x+1)
            else:
                time.sleep(3)
        print 'done'

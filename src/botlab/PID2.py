# PID.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time,signal
import lcm
import math 
import PID2

from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_laser_t
from lcmtypes import velocity_cmd_t


class PID2():
    def __init__(self, kp, ki, kd):
        # Main Gains
        self.kp = kp
        self.ki = ki
        self.kd = kd
            
        # Integral Terms
        self.iTerm = 0.0
        self.iTermMin = -0.2
        self.iTermMax = 0.2

        # Input (feedback signal)
        self.prevInput = 0.0
        self.input = 0.0

        # Reference Signals (signal and derivative!)
        self.realdotsetpoint = 0.0
        self.dotsetpoint = 0.0          
        self.setpoint = 0.0
        self.target = 0.0
        self.distance = 0
        # Output signal and limits
        self.output = 0.0
        self.outputMin = -0.25
        self.outputMax = 0.25

        # Update Rate
        self.updateRate = 0.1

        # Error Signals
        self.error = 0.0
        self.errordot = 0.0
        
        self.trim = 0.0
        self.time = 0.0
        self.done = 0
        self.end = False
        self.count = 0
    def Compute(self):
        # IMPLEMENT ME!
        # Different then last project! 
        # Now you have a speed reference also!
        # Also Update Rate can be gone here or in SetTunnings function
        # (it is your choice!) But change functions consistently
        self.setpoint = self.setpoint + (self.dotsetpoint * self.updateRate)
        self.error = self.setpoint - self.input
        self.distance = self.input
        if self.updateRate != 0:
            #self.realdotsetpoint = self.realdotsetpoint + 0.001 * self.dotsetpoint
            #if(math.fabs(self.realdotsetpoint) > math.fabs(self.dotsetpoint)):
            self.realdotsetpoint = self.dotsetpoint
            self.errordot = self.realdotsetpoint - (self.input - self.prevInput)/self.updateRate
        else:
            self.realdotsetpoint = self.dotsetpoint
            self.errordot = self.realdotsetpoint
        self.iTerm = self.iTerm + self.ki * self.error
        if self.iTerm > self.iTermMax:
            self.iTerm = self.iTermMax
        elif self.iTerm < self.iTermMin:
            self.iTerm = self.iTermMin 
        self.output = self.kp * self.error + self.kd * (self.errordot) + self.iTerm + self.trim #trim
        self.time = self.time + self.updateRate
        if self.output > self.outputMax:
            self.output = self.outputMax
        if self.output < self.outputMin:
            self.output = self.outputMin
        #if math.fabs(self.setpoint) > 0.95 * math.fabs(self.target):
        #    self.output = (self.target - self.setpoint) / (self.target*(1 - 0.95)) * self.output
        if (abs(self.distance) >= abs(self.target)):
            self.dotsetpoint = 0 
            #self.output = 0
            self.iTerm = 0
            #self.done = 1
            self.end = True
        
        if self.end == True:
            if ((self.trim > 0 and self.target>0) or (self.trim < 0 and self.target<0)) and abs(self.distance) >= abs(self.target):
                self.trim=self.trim*-0.90
            if ((self.trim < 0 and self.target>0) or (self.trim > 0 and self.target<0)) and abs(self.distance) < abs(self.target):
                self.trim=self.trim*-0.90
            self.count = self.count + 1
            if self.count > 500:
                self.done = 1
                
        self.prevInput = self.input
        
        
    # Accessory Function to Change Gains
    # Update if you are not using updateRate in Compute()
    def SetTunings(self, kp, ki, kd):
        self.kp = kp
        self.ki = ki 
        self.kd = kd

    def SetIntegralLimits(self, imin, imax):
        self.iTermMin = imin
        self.iTermMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax
                
    def SetUpdateRate(self, rateInSec):
        self.updateRate = rateInSec

    def clearSetpoint(self):
        self.setpoint = 0

class PIDController():
    def __init__(self):
     
        # Create Both PID
        self.leftCtrl =  PID2(0.0012,0.000005,0.0001)
        self.rightCtrl = PID2(0.0012,0.000005,0.0001)
        
        # LCM Subscribe
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", 
                                        self.motorFeedbackHandler)
        lcmVelSub = self.lc.subscribe("GS_VELOCITY_CMD",self.velocityHandler)
        signal.signal(signal.SIGINT, self.signal_handler)
        self.publishLaser(0)

        # Odometry 
        self.wheelDiameterMillimeters = 32.0  # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0     # separation of wheels [mm]    
        self.ticksPerRev = 16.0           # encoder tickers per motor revolution
        self.gearRatio = 30.0             # 30:1 gear ratio
        self.enc2mm = ((math.pi * self.wheelDiameterMillimeters) / 
                       (self.gearRatio * self.ticksPerRev)) 
                        # encoder ticks to distance [mm]

        # Initialization Variables
        self.InitialPosition = (0.0, 0.0)
        self.oldTime = 0.0
        self.startTime = 0.0
        self.currTick = (0,0)
        self.prevTick = (0,0)
        self.totalTick = (0,0)
        self.realTime = 0.0
        self.velScale = 1.0
        self.timeDiff = 0.0
        self.leftCtrl.setPoint = 0.0
        self.rightCtrl.setPoint = 0.0
        self.leftCtrl.trim = 0.0
        self.rightCtrl.trim = 0.0
        self.targetAngle = 0.0
        self.targetSpeed = 0.0
        self.targetDistance = 0.0
    def publishMotorCmd(self):
        cmd = maebot_diff_drive_t()
        if(self.leftCtrl.done == 0):
            cmd.motor_left_speed = self.velScale * self.leftCtrl.output
        else:
            cmd.motor_left_speed = 0
        if(self.rightCtrl.done == 0):   
            cmd.motor_right_speed = self.velScale * self.rightCtrl.output
        else:
            cmd.motor_right_speed = 0
        print cmd.motor_right_speed,cmd.motor_left_speed
        self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        # IMPLEMENT ME
    def velocityHandler(self,channel,data):
        msg = velocity_cmd_t.decode(data)
        self.targetAngle = msg.ang_vel
        self.targetDistance = msg.fwd_dis
        self.targetSpeed = msg.fwd_vel
        self.totalTick = (0,0)
        self.leftCtrl.setpoint = 0
        self.rightCtrl.setpoint = 0
        self.leftCtrl.iTerm = 0
        self.rightCtrl.iTerm = 0
        self.rightCtrl.done = 0
        self.leftCtrl.done = 0
        self.rightCtrl.count = 0
        self.leftCtrl.count = 0
        self.rightCtrl.end = False
        self.leftCtrl.end = False
        self.rightCtrl.distance = 0
        self.leftCtrl.distance = 0
        if (self.targetAngle != 0):
            DesiredDistance = math.pi * self.axleLengthMillimeters * (self.targetAngle/360.0) / self.enc2mm
            DesiredSpeed = 40 / self.enc2mm
            self.leftCtrl.SetTunings(0.0008,0.000005,0.00001)
            self.rightCtrl.SetTunings(0.0008,0.000005,0.00001)
            if(self.targetAngle > 0) :
                
                self.leftCtrl.trim = -0.155       #0.001 * 0.2 * DesiredSpeed
                self.rightCtrl.trim = 0.155      #0.001 * 0.2 * DesiredSpeed
                self.leftCtrl.target = -1 * DesiredDistance 
                self.leftCtrl.dotsetpoint = -1 * DesiredSpeed
                self.rightCtrl.target = DesiredDistance
                self.rightCtrl.dotsetpoint = DesiredSpeed
            else:
                self.leftCtrl.trim = 0.155       #0.001 * 0.2 * DesiredSpeed
                self.rightCtrl.trim = -0.155      #0.001 * 0.2 * DesiredSpeed
                self.leftCtrl.target = DesiredDistance
                self.leftCtrl.dotsetpoint = DesiredSpeed
                self.rightCtrl.target = -1 * DesiredDistance
                self.rightCtrl.dotsetpoint = -1 * DesiredSpeed
        else:
            self.leftCtrl.SetTunings(0.0012,0.00001,0.00001)
            self.rightCtrl.SetTunings(0.0012,0.00001,0.00001)
            DesiredDistance = self.targetDistance / self.enc2mm
            DesiredSpeed = self.targetSpeed / self.enc2mm
            if self.targetSpeed > 0:
                self.leftCtrl.trim = 0.154      #0.001 * 0.2 * DesiredSpeed
                self.rightCtrl.trim = 0.15      #0.001 * 0.2 * DesiredSpeed
            else:
                self.leftCtrl.trim = -0.154      #0.001 * 0.2 * DesiredSpeed
                self.rightCtrl.trim = -0.15      #0.001 * 0.2 * DesiredSpeed
            self.leftCtrl.target = DesiredDistance
            self.leftCtrl.dotsetpoint = DesiredSpeed
            self.rightCtrl.target = DesiredDistance
            self.rightCtrl.dotsetpoint = DesiredSpeed        
    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)
        self.prevTick = self.currTick
        self.currTick = (msg.encoder_left_ticks,msg.encoder_right_ticks)
        self.oldTime = self.startTime
        self.startTime = msg.utime
        if self.oldTime != 0:
            self.timeDiff = (self.startTime - self.oldTime) / 1000000.0
        if self.prevTick != (0,0):
            self.totalTick = (self.totalTick[0] + self.currTick[0] - self.prevTick[0],self.totalTick[1] + self.currTick[1] - self.prevTick[1])
        self.leftCtrl.input = self.totalTick[0] #* self.enc2mm
        self.rightCtrl.input = self.totalTick[1] #* self.enc2mm #update
        self.leftCtrl.updateRate = self.timeDiff
        self.rightCtrl.updateRate = self.timeDiff
        self.leftCtrl.Compute()
        self.rightCtrl.Compute()
        self.publishMotorCmd()
        # IMPLEMENT ME
    
    def Controller(self):

        # For now give a fixed command here
        # Later code to get from groundstation should be used

        
        while(1):
            self.lc.handle()
            


        # IMPLEMENT ME
        # MAIN CONTROLLER
            
    def publishLaser(self,x):
        # IMPLEMENT ME
        # TASK: PUBLISHES BOT_ODO_VEL MESSAGE
        msg = maebot_laser_t()
        msg.laser_power = x
        self.lc.publish("MAEBOT_LASER",msg.encode())      

        
    # Function to print 0 commands to morot when exiting with Ctrl+C 
    # No need to change 
    def signal_handler(self,signal, frame):
        print("Terminating!")
        for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0  
            self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        exit(1)

pid = PIDController()
pid.Controller()

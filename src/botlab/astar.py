#A* algorithm
import numpy as np
import heapq
N = 1
S = -1
E = -2 
W = 2
XDIM = 50
YDIM = 50   
STARTX = 25
STARTY = 25
ENDX = 0
ENDY = 25
THRESH = 0
class node:
    def __init__(self, xPos, yPos, live):
        self.xPos = xPos
        self.yPos = yPos
        self.live = live
        self.parent = None
        self.g = 0
        self.h = 0
        self.f = 0
    def updateParent(self,parent):
        self.parent = parent
class astar(object):
    def __init__(self,start,end):
        self.nodeList = []
        self.known = set()
        self.unknown = []
        heapq.heapify(self.unknown)
        self.xdim = XDIM
        self.ydim = YDIM
        self.data = np.loadtxt('scale.txt',delimiter = ',')
        self.path = [] 
        self.command = []
        self.startx = int((3000 - start[1]) / 60)
        self.starty = int(start[0] / 60)
        print self.startx,self.starty
        self.endx= int((3000 - end[1]) / 60)
        self.endy = int(end[0] / 60)
        print self.endx,self.endy
    def degrade(self,data):
        newdata = np.array()
        for x in range (0,self.xdim):
            for y in range (0,self.ydim):
                a = int(x / 10)
                b = int(y / 10)
                newdata[a][b] =  newdata[a][b] + data[x][y]
    def loadMap(self,data):
        for x in range (0,self.xdim):
            for y in range (0,self.ydim):
                print x,y
                if data[x][y] > THRESH:
                    self.nodeList.append(node(x,y,False))
                else:
                    self.nodeList.append(node(x,y,True))
                
        self.start = self.getNode(STARTX,STARTY)
        self.end = self.getNode(ENDX,ENDY)
    def createMap(self):
        data = np.loadtxt('scale.txt',delimiter = ',')
        #data = self.degrade(data)
        #print data
        for x in range (0,self.xdim):
            for y in range (0,self.ydim):
                if data[x][y] > THRESH:
                    self.nodeList.append(node(x,y,False))
                else:
                    self.nodeList.append(node(x,y,True))
                
        self.start = self.getNode(self.startx,self.starty)
        self.end = self.getNode(self.endx,self.endy)
    def getNode(self,x,y):
        return self.nodeList[x*self.xdim + y]

    def getH(self,node):
        d1 = np.square(self.end.xPos - node.xPos) + np.square(self.end.yPos - node.yPos)
        d2 = abs(self.end.xPos - node.xPos) + abs(self.end.yPos - node.yPos)
        return np.sqrt(d1)
    
    def getNeighborNodes(self,node):
        nodes = []
        if node.yPos < self.ydim - 1:
            nodes.append(self.getNode(node.xPos,node.yPos+1))
        if node.xPos < self.xdim - 1:
            nodes.append(self.getNode(node.xPos+1,node.yPos))
        if node.xPos > 0:
            nodes.append(self.getNode(node.xPos-1,node.yPos))
        if node.yPos > 0:
            nodes.append(self.getNode(node.xPos,node.yPos-1))
        return nodes

    def updateNode(self,neighbor,node):
        neighbor.g = node.g + 1
        neighbor.h = self.getH(neighbor)
        neighbor.f = neighbor.g + neighbor.h
        neighbor.parent = node
    def printPath(self):
        node = self.end
        self.path.append(node)
        while node.parent is not self.start:
            node = node.parent
            #print node.live
            #print 'path: %d,%d' % (node.xPos, node.yPos)
            self.data[node.xPos][node.yPos] = 0.5
            self.path.append(node)
        print 'generating path'
        np.savetxt('result.txt',self.data,delimiter = ',',fmt='%1.1f')
    def getPathlist(self):
        self.path.append(self.start)
        pathlist = []
        self.path.reverse()
        for node in self.path:
            pathlist.append([node.xPos*60,node.yPos*60])
        pathlist.append([self.end.xPos*60,self.end.yPos*60])
        #print pathlist
        return pathlist
    def pathCommand(self):
        self.path.reverse()
        prevnode = self.start
        orientation = 0
        preOrientation = S
        self.command = []
        index = 0
        while self.path!= []:
            moveY = (self.path[0].xPos - prevnode.xPos)
            moveX = (self.path[0].yPos - prevnode.yPos)
            orientation = moveX * W + moveY * N
            prevnode = self.path[0]
            #print orientation,self.path[0].xPos,self.path[0].yPos
            del self.path[0]
            if self.command == []:
                if abs(moveX) > 0:
                    self.command.append((0,orientation * 45))
                    index = index + 1
                    self.command.append((abs(moveX),0))
                else:
                    self.command.append((moveY,0))
                index = index + 1
                preOrientation = orientation
                #print 'initial'
                #print self.command[index-1],self.command[index-2]
            elif abs(preOrientation) == abs(orientation):
                print self.command[index - 1]
                pMove = self.command[index - 1][0] + abs(moveX + moveY)
                self.command[index - 1] = (pMove,0)
                if pMove < 0:
                    preOrientation = orientation
                #print self.command[index-1]
            else:
                if orientation == N and preOrientation  == E:
                    self.command.append((0,90))
                elif orientation == N and preOrientation  == W:
                    self.command.append((0,-90)) 
                elif orientation == S and preOrientation  == E:
                    self.command.append((0,-90)) 
                elif orientation == S and preOrientation  == W:
                    self.command.append((0,90))
                elif orientation == E and preOrientation  == N:
                    self.command.append((0,-90))
                elif orientation == E and preOrientation  == S:
                    self.command.append((0,90)) 
                elif orientation == W and preOrientation  == N:
                    self.command.append((0,90)) 
                elif orientation == W and preOrientation  == S:
                    self.command.append((0,-90))  
                index = index + 1
                if preOrientation == N or preOrientation == S:
                    self.command.append((abs(moveX),0))
                else:
                    self.command.append((abs(moveY),0))
                preOrientation = orientation
                index = index + 1        
        return self.command




    def compute(self):
        heapq.heappush(self.unknown, (self.start.f,self.start))
        while len(self.unknown):
            f, node = heapq.heappop(self.unknown)
            self.known.add(node)
            if node is self.end:
                self.printPath()
                break
            neighborList = self.getNeighborNodes(node)
            for neighbor in neighborList:
                if neighbor.live and neighbor not in self.known:
                    if (neighbor.f,neighbor) in self.unknown:
                        if neighbor.g > node.g + 1:
                            self.updateNode(neighbor,node)
                    else:
                        self.updateNode(neighbor,node)
                        heapq.heappush(self.unknown,(neighbor.f,neighbor))
        print "error"
'''a = astar()
a.createMap()
a.compute()   '''


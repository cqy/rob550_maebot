import numpy as np
#import matplotlib.pyplot as plt
DIM = 300
DIMNEW = 50
SCALEFACTOR = DIM/DIMNEW
data = np.loadtxt('data1.txt',delimiter = ',')
#print data
newdata = np.zeros((DIMNEW,DIMNEW))
for x in range (0,DIM):
    for y in range (0,DIM):
        a = int(x / SCALEFACTOR)
        b = int(y / SCALEFACTOR)
        newdata[a][b] =  newdata[a][b] + data[x][y]  
for x in range (0,DIMNEW):
    for y in range (0,DIMNEW):
        if newdata[x][y] > 200 * SCALEFACTOR * SCALEFACTOR:
            newdata[x][y] = 0
        else:
            newdata[x][y] = 1
            
np.savetxt('scale.txt',newdata,delimiter = ',',fmt='%1.1d')


# README #

Welcome to the ROBOTICS 550 Lab course maebot repository.1

### How do I get set up? ###

Follow the instructions in the `INSTALL` file provided in the root directory of the repo.

### Who do I talk to? ###

* Instructors:
    + Prof. Ella Atkins <ematkins@umich.edu>
    + Prof. Shai Revzen <shrevzen@umich.edu>
* Other community or team contact:
    + Lab Engineer: Mr. Peter Gaskell <pgaskell@umich.edu>
